
/**
 *
 * @author Han Yu
 */
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private final boolean[][] grid;
    private final WeightedQuickUnionUF wquuf;
    private final int gridsize;
    private int numOfOpenSites;
    private int topVirtualNode;
    private boolean isTopVirtualNodeSet; 

    /**
     * create n-by-n grid, with all sites blocked
     *
     * @param n
     */
    public Percolation(int n) {
        // validate input n
        if (n <= 0) {
            throw new IllegalArgumentException("Grid size has to be larger than 0!");
        }
        // initialize gridsize
        this.gridsize = n;
        // initialize grid, with all sites blocked (set to false)
        this.grid = new boolean[this.gridsize][this.gridsize];
        for (int i = 0; i < this.gridsize; i++) {
            for (int j = 0; j < this.gridsize; j++) {
                this.grid[i][j] = false;
            }
        }
        // initializes an empty union–find data structure with n sites 0 through n-1
        this.wquuf = new WeightedQuickUnionUF(this.gridsize * this.gridsize);
        // initializes number of open sites to zero
        this.numOfOpenSites = 0;
        this.isTopVirtualNodeSet = false; 
    }

    /**
     * connect with site on the right
     *
     * @param row
     * @param col
     */
    private void connectWithRightSite(int row, int col) {
        int p = convert2DArrayIndxTo1DArrayIndx(row, col);
        int q = convert2DArrayIndxTo1DArrayIndx(row, col + 1);
        if (isOpen(row, col + 1)) {
            this.wquuf.union(p, q);
        }
    }

    /**
     * connect with site on the left
     *
     * @param row
     * @param col
     */
    private void connectWithLeftSite(int row, int col) {
        int p = convert2DArrayIndxTo1DArrayIndx(row, col);
        int q = convert2DArrayIndxTo1DArrayIndx(row, col - 1);
        if (isOpen(row, col - 1)) {
            this.wquuf.union(p, q);
        }
    }

    /**
     * connect with site above
     *
     * @param row
     * @param col
     */
    private void connectWithUpSite(int row, int col) {
        int p = convert2DArrayIndxTo1DArrayIndx(row, col);
        int q = convert2DArrayIndxTo1DArrayIndx(row - 1, col);
        if (isOpen(row - 1, col)) {
            this.wquuf.union(p, q);
        }
    }

    /**
     * connect with site below
     *
     * @param row
     * @param col
     */
    private void connectWithDownSite(int row, int col) {
        int p = convert2DArrayIndxTo1DArrayIndx(row, col);
        int q = convert2DArrayIndxTo1DArrayIndx(row + 1, col);
        if (isOpen(row + 1, col)) {
            this.wquuf.union(p, q);
        }
    }

    /**
     * connect with top virtual node
     *
     * @param row
     * @param col
     */
    private void connectWithTopVirtualNode(int row, int col) {
        if (row == 1) {
            int p = convert2DArrayIndxTo1DArrayIndx(row, col);
            this.wquuf.union(p, this.topVirtualNode);
        }
    }

    /**
     * open site (row, col) if it is not open already
     *
     * @param row
     * @param col
     */
    public void open(int row, int col) {
        int gridRowIdx, gridColIdx;
        if (row > this.gridsize || row <= 0) {
            throw new IllegalArgumentException("Input row index is outside its prescribed range" + "[1, " + this.gridsize + "], "
                    + "input row = " + row);
        } else if (col > this.gridsize || col <= 0) {
            throw new IllegalArgumentException("Input column index is outside its prescribed range" + "[1, " + this.gridsize + "], "
                    + "input col = " + col);
        } else {
            gridRowIdx = row - 1;
            gridColIdx = col - 1;
        }
        if (!isOpen(row, col)) {
            this.grid[gridRowIdx][gridColIdx] = true;
            this.numOfOpenSites += 1;
        }
        if(row == 1 && !this.isTopVirtualNodeSet){
            this.topVirtualNode = convert2DArrayIndxTo1DArrayIndx(row, col); 
            this.isTopVirtualNodeSet = true;
        }
        if (this.gridsize == 1) {
            connectWithTopVirtualNode(row, col);
            return;
        }

        if (this.gridsize == 2) {
            if (gridColIdx == 0) {
                connectWithRightSite(row, col);
            } else {
                connectWithLeftSite(row, col);
            }
            if (gridRowIdx == 0) {
                connectWithDownSite(row, col);
            } else {
                connectWithUpSite(row, col);
            }
        } else {
            if (gridColIdx == 0) {
                connectWithRightSite(row, col);
            } else if (gridColIdx == this.gridsize - 1) {
                connectWithLeftSite(row, col);
            } else {
                connectWithLeftSite(row, col);
                connectWithRightSite(row, col);
            }
            if (gridRowIdx == 0) {
                connectWithDownSite(row, col);
                connectWithTopVirtualNode(row, col);
            } else if (gridRowIdx == this.gridsize - 1) {
                connectWithUpSite(row, col);
            } else {
                connectWithUpSite(row, col);
                connectWithDownSite(row, col);
            }
        }
    }

    /**
     * check if site(row, col) is open
     *
     * @param row
     * @param col
     * @return
     */
    public boolean isOpen(int row, int col) {
        boolean rtn = false;
        int gridRowIdx, gridColIdx;
        if (row > this.gridsize || row <= 0) {
            throw new IllegalArgumentException("Input row index is outside its prescribed range" + "[1, " + this.gridsize + "], "
                    + "input row = " + row);
        } else if (col > this.gridsize || col <= 0) {
            throw new IllegalArgumentException("Input column index is outside its prescribed range" + "[1, " + this.gridsize + "], "
                    + "input col = " + col);
        } else {
            gridRowIdx = row - 1;
            gridColIdx = col - 1;
            rtn = this.grid[gridRowIdx][gridColIdx];
        }
        return rtn;
    }

    /**
     * check if site(row, col) is fully open
     *
     * @param row
     * @param col
     * @return
     */
    public boolean isFull(int row, int col) {
        boolean rtn = false;
        if (isOpen(row, col)) {
            if (this.gridsize == 1) {
                rtn = true;
                return rtn;
            }

            int p = convert2DArrayIndxTo1DArrayIndx(row, col);
            if (this.wquuf.connected(p, this.topVirtualNode)) {
                rtn = true;
                return rtn;
            }
        }
        return rtn;
    }

    /**
     * calculate number of open sites
     *
     * @return
     */
    public int numberOfOpenSites() {
        return this.numOfOpenSites;
    }

    /**
     * check if the system percolates
     *
     * @return
     */
    public boolean percolates() {
        boolean rtn = false;
        if (this.gridsize == 1) {
            if (this.grid[0][0]) {
                rtn = true;
            }
            return rtn;
        }
        if (this.isTopVirtualNodeSet) {
            for(int col = 1; col <= this.gridsize; col++){
                int p = convert2DArrayIndxTo1DArrayIndx(this.gridsize, col); 
                if(isOpen(this.gridsize, col)){
                    if(this.wquuf.connected(p, this.topVirtualNode)){
                        rtn = true; 
                        return rtn; 
                    }
                }
            }
        }
        return rtn;
    }

    /**
     * convert 2D grid index to 1D array index
     *
     * @param row
     * @param col
     * @return
     */
    private int convert2DArrayIndxTo1DArrayIndx(int row, int col) {
        int rtn = 0;
        if (row > this.gridsize || row <= 0) {
            throw new IllegalArgumentException("Input row index is outside its prescribed range" + "[1, " + this.gridsize + "], "
                    + "input row = " + row);
        } else if (col > this.gridsize || col <= 0) {
            throw new IllegalArgumentException("Input column index is outside its prescribed range" + "[1, " + this.gridsize + "], "
                    + "input col = " + col);
        } else {
            rtn = (row - 1) * this.gridsize + (col - 1);
        }
        return rtn;
    }

}
