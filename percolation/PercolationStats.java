
/**
 *
 * @author Han Yu
 */
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private final int girdSize;
    private final int trials;
    private final double[] results;
    private final double mean;
    private final double stddev;
    private final double confidenceLo;
    private final double confidenceHi;

    /**
     * perform trials independent experiments on an n-by-n grid
     *
     * @param n
     * @param trials
     */
    public PercolationStats(int n, int trials) {
        if (n <= 0) {
            throw new IllegalArgumentException("Size of the grid should be larger than 0!");
        }
        if (trials <= 0) {
            throw new IllegalArgumentException("Number of the trials should be larger than 0!");
        }
        this.girdSize = n;
        this.trials = trials;
        this.results = new double[this.trials];
        for (int trial = 0; trial < this.trials; trial++) {

            Percolation perc = new Percolation(n);
            while (!perc.percolates()) {
                int row = StdRandom.uniform(1, this.girdSize + 1);
                int col = StdRandom.uniform(1, this.girdSize + 1);
                perc.open(row, col);
            }
            this.results[trial] = ((double) perc.numberOfOpenSites()) / ((double) this.girdSize * this.girdSize);
        }

        this.mean = StdStats.mean(results);
        this.stddev = StdStats.stddev(results);
        this.confidenceLo = mean - 1.96 * stddev / Math.sqrt(trials);
        this.confidenceHi = mean + 1.96 * stddev / Math.sqrt(trials);

    }

    /**
     * sample mean of percolation threshold
     *
     * @return
     */
    public double mean() {
        return this.mean;
    }

    /**
     * sample standard deviation of percolation threshold
     *
     * @return
     */
    public double stddev() {
        return this.stddev;
    }

    /**
     * low endpoint of 95% confidence interval
     *
     * @return
     */
    public double confidenceLo() {
        return this.confidenceLo;
    }

    /**
     * high endpoint of 95% confidence interval
     *
     * @return
     */
    public double confidenceHi() {
        return this.confidenceHi;
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats percStat = new PercolationStats(n, trials);
        StdOut.println("mean = " + percStat.mean());
        StdOut.println("stddev = " + percStat.stddev());
        StdOut.println("95% confidence interval = [" + percStat.confidenceLo() + ", " + percStat.confidenceHi() + "]");
    }

}
